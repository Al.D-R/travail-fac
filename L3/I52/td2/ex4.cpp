#include <iostream>

class Vec {
  int dim;
  float *comp;

public:
  Vec(); // Methode
  Vec(int d); // Methode
  Vec(int d, float *cmp); // Methode
  Vec(const Vec& v); // Methode
  ~Vec(); // Methode

  Vec& operator=(const Vec& v); // Methode

  float& operator[](int i); // Methode
  Vec operator+(const Vec& v) const; // Methode
  Vec operator+(float x) const; // Methode
  friend Vec operator+(float x, const Vec& v); // Amie
  friend std::ostream& operator<<(std::ostream& out, const Vec& v); // Amie
};

Vec::Vec(): dim(0), comp(nullptr) {}
Vec::Vec(int d): dim(d) {
  comp = new float[dim];
}
Vec::Vec(int d, float *cmp): dim(d) {
  comp = new float[dim];
  for (int i = 0; i < dim; i++)
    comp[i] = cmp[i];
}
Vec::Vec(const Vec& v): dim(v.dim) {
  comp = new float[dim];
  for (int i = 0; i < dim; i++)
    comp[i] = v.comp[i];
}
Vec::~Vec() {
  delete comp;
}

Vec& Vec::operator=(const Vec& v) {
  if (this != &v) {
    dim = v.dim;
    if (comp != nullptr)
      delete comp;
    comp = new float[dim];
    for (int i = 0; i < dim; i++)
      comp[i] = v.comp[i];
  }
  return *this;
}

float& Vec::operator[](int i) {
  return comp[i];
}

Vec Vec::operator+(const Vec& v) const {
  int dimMin{ dim < v.dim ? dim : v.dim };
  int dimMax{ dim < v.dim ? v.dim : dim };
  Vec res{ dimMax };
  for (int i = 0; i < dimMin; i++)
    res[i] = comp[i] + v.comp[i];
  for (int i = dimMin; i < dimMax; i++)
    res[i] = dim < v.dim ? v.comp[i] : comp[i];
  return res;
}

Vec Vec::operator+(float x) const {
  float tab[dim + 1];
  for (int i = 0; i < dim; i++)
    tab[i] = comp[i];
  tab[dim] = x;
  return Vec(dim + 1, tab);
}
Vec operator+(float x, const Vec& v) {
  return v + x;
}

std::ostream& operator<<(std::ostream& out, const Vec& v) {
  out << "(";
  for (int i = 0; i < v.dim; i++) {
    out << v.comp[i];
    if (i < v.dim - 1)
      out << ",";
  }
  out << ")";
  return out;
}


int main() {

  float tab1[3] = { 2.0f, 5.0f, 7.0f };
  float tab2[3] = { 6.0f, 4.0f, 9.0f };
  float tab3[3] = { 8.0f, 5.0f, 2.0f };

  Vec v1{3, tab1}, v2{3, tab2}, v3{3, tab3}, v4{v2}, v5;

  std::cout << v1 << std::endl
            << v2 << std::endl
            << v3 << std::endl
            << v4 << std::endl
            << v5 << std::endl;
  v5 = v3 = v2;
  std::cout << v2 << " " << v3 << " " << v5 << std::endl;
  std::cout << v1 + v2 + v3 << std::endl;

  v1[1] = 12.0f;
  std::cout << v1 << std::endl;

  return 0;
}
