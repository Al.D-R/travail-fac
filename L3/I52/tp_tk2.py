from tkinter import *

def on_nouveau(): global canvas; canvas.delete("all")

def on_ouvrir():
    print("OUVRIR")

def on_sauvegarder():
    print("SAUVEGARDER")

# Fenetre principale
root = Tk()
root.option_add('*tearOff', False)

# Barre de menus
menu_bar = Frame(root)
menu_bar.pack(anchor=NW, fill="x")

# Menus
file_menu_btn = Menubutton(menu_bar, text="Fichier")
file_menu_btn.pack(side=LEFT)
help_menu_btn = Menubutton(menu_bar, text="Aide")
help_menu_btn.pack(side=RIGHT)

# Menu Fichier
file_menu = Menu(file_menu_btn)
file_menu.add_command(label="Nouveau", command = on_nouveau)
file_menu.add_command(label="Ouvrir", command = on_ouvrir)
file_menu.add_command(label="Sauvegarder", command = on_sauvegarder)
file_menu.add_command(label="Quitter", command=exit)
file_menu_btn["menu"] = file_menu

# Canvas
canvas = Canvas(root, bg = "red")
canvas.pack(expand=1, fill = BOTH)

# Status bar
status_bar = Frame(root, bg="#87CEFA")
status_bar.pack(expand=0, fill="x")
indicator = Label(status_bar, text="0", bg="#87CEFA")
indicator.pack(anchor=SW)

root.mainloop()
