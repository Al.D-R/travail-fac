#include <iostream>

/* Bonus sympa */
template<typename T>
void permuter(T& a, T& b) {
  T tmp = a;
  a = b;
  b = tmp;
}

/*
void permuter(int& a, int& b) {
  int tmp = a;
  a = b;
  b = tmp;
}

void permuter(int* a, int* b) {
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void permuter(float& a, float& b) {
  float tmp = a;
  a = b;
  b = tmp;
}

void permuter(float* a, float* b) {
	float tmp = *a;
	*a = *b;
	*b = tmp;
}
*/

int main() {
  int a = 2, b = 3;
  permuter<int>(a, b);
  std::cout << a << " " << b << std::endl;
  a = 2;
  b = 3;
  float x = 2.4, y = 6.98;
  permuter<float>(x, y);
  std::cout << x << " " << y << std::endl;

  return 0;

}
