import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from math import log

def entropie(l, denom):
    """calcule l'entropie a partir d'une liste"""
    res = 0
    for x in l:
        if x != 0:
            res -= (x / denom) * log((x / denom), 2)
    return res

l = [0 for x in range(256)]
Im = Image.open('lena.jpeg')
Imarray = np.asarray(Im)

nbval = 0
for line in Imarray:
    for pix in line:
        l[pix] += 1
        nbval += 1

print(entropie(l, nbval))

"""
Im = Image.open('lena.jpeg')
Imarray = np.asarray(Im)

plt.figure()
plt.imshow(Imarray, cmap='gray')

xe = np.asarray(range(np.amax(Imarray[:])+2))
H1, xe = np.histogram(Imarray.reshape(-1), bins=xe)
P1 = H1/Imarray.size
plt.figure()
plt.plot(P1)
"""


