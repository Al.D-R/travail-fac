(** Passe les entiers dans la liste au denominateur d *)
let to_proba d l = List.map (fun x -> float_of_int x /. float_of_int d) l

(** Calcule de l'entropie d'une liste *)
let entropie l = List.fold_left (fun acc x -> acc -. x *. Float.log2 x) 0. l


(** Calcule de l'information mutuelle grace aux entropies de p et q
 *  ainsi que l'entropie jointe (r) *)
let im p q r =
    entropie p +. entropie q -. entropie r

(** Calcule de la divergence KL de 2 listes *)
let div_kl p q =
    let f = fun i x ->
        let x' = List.nth q i in
        x *. Float.log2 (x /. x')
    in p |> List.mapi f |> List.fold_left (+.) 0.

let () =
  let open Float in
  let prob = Array.make (7*4) (1. /. (7. *. 4.)) |> Array.to_list in
  let e = entropie prob in
  print_float e |> print_newline
