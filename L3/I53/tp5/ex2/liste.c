#include "liste.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int inserer(char *nom, list_symb **ptr) {
  if (ptr == NULL) {
    fprintf(stderr, "Liste nulle\n");
    return -1;
  }

  if (*ptr == NULL) {
    list_symb *res = malloc(sizeof(list_symb));
    strcpy(res->symb, nom);
    res->count = 1;
    *ptr = res;
    return 0;
  }

  list_symb *it = *ptr;
  list_symb *last = NULL;

  while (it != NULL) {
    if (!strcmp(it->symb, nom)) {
      it->count++;
      return 0;
    }
    last = it;
    it = it->suiv;
  }

  list_symb *res = malloc(sizeof(list_symb));
  strcpy(res->symb, nom);
  res->count = 1;
  last->suiv = res;

  return 1;

}

void print_list(list_symb *ptr) {

  list_symb *it = ptr;
  printf("[");
  while (it != NULL) {
    printf("(%s, %d)", it->symb, it->count);
    if (it->suiv != NULL)
      printf(", ");
    it = it->suiv;

  }
  printf("]\n");

}

void free_list(list_symb *ptr) {
  list_symb *it = ptr, *next = NULL;
  while (it != NULL) {
    next = it->suiv;
    free(it);
    it = next;
  }
}
