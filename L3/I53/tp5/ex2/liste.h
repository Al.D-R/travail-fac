#ifndef _LISTE_H_
#define _LISTE_H_

typedef struct list_symb {
  char symb[128];
  int count;
  struct list_symb *suiv;
} list_symb;

int inserer(char *nom, list_symb **ptr);

void free_list(list_symb *ptr);

void print_list(list_symb *ptr);

#endif
