%{
  /*Prologue*/
  #include <stdio.h>
  #include <string.h>

  #include "liste.h"

  list_symb *l = NULL;

%}

%option nounput
%option noinput

/*Définitions*/
BLANC [ \t\n]
LETTRE [a-zA-Z]
MOT {LETTRE}+
%%

{MOT} {
  printf("Mot: %s, longueur: %d\n", yytext, yyleng);
  inserer(yytext, &l);

}
{BLANC}
.
%%
int main(int argc, char **argv) {

  yylex();

  print_list(l);
  free_list(l);
  return 0;
}
