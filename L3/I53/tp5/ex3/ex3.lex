%{
  /*Prologue*/
  #include <stdio.h>
  #include <string.h>

  int indentMesure = -1;
  int indentLevel = 0;
  int funcall = 0;
  int loopDeepness = 0;
  int loopDeepnessMax = 0;

%}

%option nounput
%option noinput

/*Définitions*/
BLANC   [ \t\n]
LPAR    [\(]
RPAR    [\)]
LETTRE [a-zA-Z_]
COMMA  [\,]

SPACE  [ ]
MOT {LETTRE}+

INDENT ^{SPACE}*
LOOP "while"|"for"
FUNCALL {MOT}{SPACE}*{LPAR}


%%

{INDENT} {
  printf("INDENTATION, longeure: %d\n", yyleng);
  if (indentMesure == -1 && yyleng > 0)
    indentMesure = yyleng;
  if (yyleng == (indentLevel + indentMesure))
    indentLevel += indentMesure;
  else if (yyleng == (indentLevel - indentMesure))
    indentLevel -= indentMesure;
  else if (yyleng == indentLevel);
  else {
    fprintf(stderr, "Erreur d'indentation\n");
    exit(1);
  }
  printf("indentmesure: %d, indentlevel: %d\n", indentMesure, indentLevel);
}

{LOOP} {
  printf("LOOP\n");
}

{MOT} {
  printf("MOT\n");
}

{FUNCALL} {
  printf("FUNCALL\n");
  funcall++;
}


{BLANC}
.
%%
int main(int argc, char **argv) {

  yylex();

  return 0;
}
