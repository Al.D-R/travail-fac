%{
  /*Prologue*/
  #include <stdio.h>
  #include <string.h>

  int longmax = 0;
  char motlepluslong[256];
  int numLigneMax = 1;
  int numColonne = 0;

  int numLigne = 1;
  long lastNewLine = 0;

  int sum = 0;
%}

%option nounput
%option noinput

/*Définitions*/
BLANC    [ \t]
NEWLINE  [\n]
OTHER    .
LETTRE   [a-zA-Z]
MOT      {LETTRE}+
NOMBRE   [0-9]+

%%

{NOMBRE} {
  sum += atoi(yytext);
}

{NEWLINE} {
  numLigne++;
  lastNewLine = ftell(yyin);
}

{MOT} {
  if (yyleng > longmax){
    longmax = yyleng;
    strcpy(motlepluslong, yytext);
    printf("\n%s",yytext);
    numLigneMax = numLigne;
    numColonne = ftell(yyin) - lastNewLine;
  }
}
.

%%
int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Erreur, veuillez specifier un fichier a analyser\n");
    exit(1);
  }

  FILE *f = fopen(argv[1], "r");
  if (!f) {
    fprintf(stderr, "Erreur lors de l'ouverture du fichier: %s\n", argv[1]);
    exit(2);
  }

  yyin = f;
  yylex();

  fclose(f);

  printf("\nMot le plus long: %s, de longueur: %d\n", motlepluslong, longmax);
  printf("Ligne: %d, colonne: %d\n", numLigneMax, numColonne);
  printf("Somme: %d\n", sum);
  return 0;
}
