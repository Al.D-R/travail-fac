#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "afn.h"

/*
 * FUNCTION: afn_init
 * ------------------
 * initialise et retourne un AFN dont les états sont numérotés de 0 à `Q`
 * ajoute le symbole '&' en début d'alphabet si celui-ci n'y est pas déjà
 *
 * param:
 *        Q  - plus grand état de l'automate
 *        nbInitaux - nombre d'états initiaux
 *        listInitiaux - tableau de `nbInitiaux` entiers représentant les états initiaux
 *        nbFinals - nombre d'états finals
 *        listFinals - tableau de `nbFinals` entiers représentant les états finals
 *        Sigma - alphabet de l'automate
 *
 * return:
 *        un AFN dont la tableau de transition est allouée mais vide
 */
AFN  afn_init(int Q, int nbInitiaux, int * listInitiaux, int nbFinals, int * listFinals, char *Sigma){
  AFN A;
  if ( (A=malloc(sizeof(struct AFN))) == NULL){
    printf("malloc error A");
    exit(1);
  }
  A->Q = Q;

  A->lenI = nbInitiaux;
  if ( (A->I = malloc(sizeof(int)*nbInitiaux)) == NULL){
    printf("malloc error A->I");
    exit(1);
  }
  for (int i=0; i<nbInitiaux; i++) A->I[i] = listInitiaux[i];

  A->lenF = nbFinals;
  if ( (A->F = malloc(sizeof(int)*nbFinals)) == NULL){
    printf("malloc error A->F");
    exit(1);
  }
  for (int i=0; i<nbFinals; i++) A->F[i] = listFinals[i];


  A->lenSigma = strlen(Sigma);
  if (Sigma[0] != '&') A->lenSigma++;
  if ( (A->Sigma = malloc(sizeof(char)*(A->lenSigma+1)))==NULL){
    printf("malloc error A->Sigma");
    exit(1);
  }
  if (Sigma[0] != '&'){
    strcpy(A->Sigma+1,Sigma);
    A->Sigma[0]='&';
  } else {
    strcpy(A->Sigma,Sigma);
  }
  for (int i=0; i<MAX_SYMBOLES; i++) A->dico[i]=-1;
  for (int i=0; i<A->lenSigma; i++) A->dico[A->Sigma[i]-ASCII_FIRST]=i;

  if ((A->delta = malloc( sizeof(int***)*(Q+1)))==NULL){
    printf("malloc error A->Sigma");
    exit(1);
  }
  for (int q=0; q<A->Q+1; q++){
    if( (A->delta[q] = malloc( sizeof(int **)*A->lenSigma))==NULL){
      printf("malloc error A->Sigma[%d]", q);
      exit(1);
    }
    for (int s=0; s<A->lenSigma; s++)	A->delta[q][s]=NULL;
  }
  return A;
}

/*
 * FUNCTION: afn_print
 * -------------------
 * affiche l'AFN `A`
 *
 * param:
 *        A  - un AFN
 */
void afn_print(AFN A){
  printf("Q = {0,..,%d}\n", A->Q);
  printf("I = {");
  for (int i=0; i<A->lenI; i++) printf("%d,",A->I[i]);
  printf("\b}\n");

  printf("F = {");
  for (int i=0; i<A->lenF; i++) printf("%d,",A->F[i]);
  printf("\b}\n");

  int state_size = (int)(ceil(log10( (double)A->Q)));
  int padding = (state_size>=5)? (state_size-5)/2+1: 1;
  int first_column_size = state_size>=5 ? state_size+2 : 7;
  int max_cell_size = 0;

  for (int q=0; q<A->Q; q++){
    for (int s=0; s<A->lenSigma; s++){
      if (A->delta[q][s]!=NULL){
	int cell_size = 0;

	while (A->delta[q][s][cell_size]!=-1) cell_size++;
	max_cell_size = (max_cell_size < cell_size ? cell_size : max_cell_size);
      }
    }
  }
  int total_cell_size = max_cell_size*(state_size+1)+1;

  int line_length = first_column_size+1+(total_cell_size+1)*A->lenSigma;
  char * line = malloc(sizeof(char)*(line_length+2));
  for (int i=0; i<=line_length; i++) line[i]='-';
  line[line_length+1]='\0';
  printf("%s\n",line);
  printf("|%*sdelta |", padding, "");
  for (int i=0; i<A->lenSigma; i++) printf("%*c |", total_cell_size-1, A->Sigma[i]);
  printf("\n");
  printf("%s\n",line);

  char *buffer = malloc(sizeof(char)*(total_cell_size+1));
  for (int q=0; q<A->Q+1; q++){
    printf("|%*d |", padding+5, q);
    for (int i=0; i<A->lenSigma; i++){
      int s = A->dico[A->Sigma[i]-ASCII_FIRST];
      if (A->delta[q][s] != NULL){
	int j=0;
	buffer[0]='{';
	buffer[1]='\0';
	while (A->delta[q][s][j]!=-1) {
	  sprintf(buffer,"%s%d,",buffer, A->delta[q][s][j++]);
	}
	printf("%*s\b}|", total_cell_size ,buffer );
      } else {
	printf("%*s|",total_cell_size,"");
      }
    }
    printf("\n");
    printf("%s\n",line);
  }
  free(buffer);
  free(line);
}

/*
 * FUNCTION: afn_free
 * -------------------
 * libère la mémoire de l'AFN `A` initialisé par la fonction afn_init
 *
 * param:
 *        A  - un AFN
 */
void afn_free(AFN A){
  free(A->I);
  free(A->F);
  free(A->Sigma);
  for (int q=0; q<A->Q+1; q++){
    for (int s=0; s<A->lenSigma; s++){
      if (A->delta[q][s]!=NULL)
	free(A->delta[q][s]);
    }
    free(A->delta[q]);
  }
  free(A->delta);
  free(A);
}


/*
 * FUNCTION: afn_ajouter_transition
 * --------------------------------
 * ajoute la transition  `q1` -- `s` --> `q2` à l'automate `A` où l'ensemble des transitions
 * partant de l'état `q1` et étiquetées par le symbole `s` delta[q][s] est un tableau
 * d'entiers trié dans l'ordre croissant et se terminant par -1, NULL si vide
 *
 * param:
 *        A  - un AFN
 *        q1 - état de départ de la transition
 *        s  - étiquette de la transition
 *        q2 - état d'arrivée de la transition
 */
void afn_ajouter_transition(AFN A, int q1, char s, int q2){

  int q = A->dico[s-ASCII_FIRST];
  if (A->delta[q1][q] == NULL) { //si il n'y a pas de valeur, on alloue de la place.
    A->delta[q1][q] = malloc(2 * sizeof(int));
    A->delta[q1][q][0] = q2;
    A->delta[q1][q][1] = -1; //indicateur de fin de tableau
  } else { //sinon
    int l = 0;
    while (A->delta[q1][q][l] != -1) { //o parcourt le tableau jusqu'à ce qu'on obtienne la valeur -1
      l++;
    }
    l+=2;//on obtient donc la longeur désiré pour ajouter une nouvelle valeur
    int* tmp = realloc(A->delta[q1][q],l); //on réaloue la place necessaire
    if (tmp == NULL) //une sécurité pour éviter un warning
      return;
    A->delta[q1][q] = tmp;
    A->delta[q1][q][l-1] = -1;
    A->delta[q1][q][l-2] = q2; //realloc rajoute de la place mais garde les anciennes valeurs.
  }
}

/*
 * FUNCTION: afn_finit
 * ------------------
 * initialise et renvoie un AFN à partir du fichier `file` écrit au format:
 *
 *  'nombre_etat
 *  'nombre_etats_initiaux
 *  'etat_initial_1 etat_initial_2 ... etat_initial_n
 *  'nombre_etats_finals
 *  'etat_final_1 etat_final_2 ... etat_final_n
 *  'alphabet
 *  'etat_i1 symbole_k1 etat_j1
 *  'etat_i2 symbole_k2 etat_j2
 *  '...
 *  'etat_in symbole_kn etat_jn
 *
 * param :
 *         file - un nom de fichier
 * return:
 *         un AFN complet
 */
AFN afn_finit(char *file) {

 FILE *fp;
 char buf[260];
 fp = fopen( file , "r");

 if(!fp){
   printf("error file\n");
   exit(1);
  }

  int Q = atoi(fgets(buf, sizeof(buf), fp)); //nombre_etat
  int nbInitiaux = atoi(fgets(buf, sizeof(buf), fp)); //nb etat_initiaux
  int* listInitiaux = (int*) malloc(nbInitiaux * sizeof(int)); //etats initiaux 1->n

  int cursor = 0;
  char numb[10];
  memset(&numb[0], 0, sizeof(numb));
  fgets(buf, sizeof(buf), fp);

  //on parcourt la ligne, on met chaque charactère dans numb, et dès qu'on a un espace on
  //ajoute dans le tableau la valeur de numb. On répète jusqu'à la fin de la ligne.
  for (int i = 0; i < nbInitiaux; i++){
    while ( (buf[cursor] != ' ') && (buf[cursor] != '\n') )
    {
      strncat(numb,&buf[cursor],1);
      cursor++;
    }
    listInitiaux[i] = atoi(numb);
    memset(&numb[0], 0, sizeof(numb));
    cursor++;
  }

////

  int nbFinals = atoi(fgets(buf, sizeof(buf), fp)); //nombre_etats_finals
  int* listFinals = (int*) malloc(nbFinals * sizeof(int)); //etat_final_1->n
  cursor = 0;
  memset(&numb[0], 0, sizeof(numb));
  fgets(buf, sizeof(buf), fp);

  //Comme pour initiaux, on parcourt la ligne, on met chaque charactère dans numb, et dès qu'on a un espace on
  //ajoute dans le tableau la valeur de numb. On répète jusqu'à la fin de la ligne.
  for (int i = 0; i < nbFinals; i++){
    while ( (buf[cursor] != ' ') && (buf[cursor] != '\n') )
    {
      strncat(numb,&buf[cursor],1);
      cursor++;
    }
    listFinals[i] = atoi(numb);
    memset(&numb[0], 0, sizeof(numb));
    cursor++;
  }


 char Sigma[256];
 fscanf(fp,"%s", Sigma); // alphabet

 AFN A;
 A = afn_init(Q,nbInitiaux,listInitiaux,nbFinals,listFinals,Sigma);

 int e1,e2;
 char c;
 while(fscanf(fp, "%d %c %d ", &e1, &c, &e2) != EOF) //transitions tq le fichier n'est pas fini
 {
   afn_ajouter_transition(A, e1, c , e2);
 }

 fclose(fp);
 return A;

}

struct Node {
    int data;
    struct Node *next;
} ;
struct Node* top = NULL;

void push(int value) {
    struct Node *newNode;
    newNode = (struct Node *)malloc(sizeof(struct Node));
    newNode->data = value; // Assigner la valeur à la pile
    if (top == NULL) {
        newNode->next = NULL;
    } else {
        newNode->next = top; // Mettre le node au sommet de la pile
    }
    top = newNode;

}

int pop() {
    if (top == NULL) {
        printf("\nPile vide\n");
    } else {
        struct Node *tempo = top;
        int tempo_data = top->data;

        if (top->next != NULL)
        {
          top = top->next;
          free(tempo);
          return tempo_data;
        }
        else
        {
          top = top->next;
          return tempo_data;
        }
    }
}

void display() {
    // Afficher les éléments de la pile
    if (top == NULL) {
        printf("\nPile vide\n");
    } else {
        printf("La pile est: \n");
        struct Node *temp = top;
        while (temp->next != NULL) {
            printf("%d--->", temp->data);
            temp = temp->next;
        }
        printf("%d--->NULL\n\n", temp->data);
    }
}

int AlreadyIn(int* res, int q) {
  int k = 0;
  while (res[k] != -1) {
    if (res[k] == q)
      return 1;
    k++;
  }
  return 0;
}

/*
 * FUNCTION: afn_epsilon_fermeture
 * -------------------------------
 * renvoie un pointeur vers l'epsilon fermeture de l'ensemble d'états `R`
 *
 * param:
 *        A - un AFN
 *        R - un ensemble d'états sous forme de tableau d'entiers triés par ordre croissant
 * terminant par -1, NULL si vide
 */
 //pour l'afn en exemple sur ddg avec 2 et 8 renvoyer 2 4 6 8 11 (-1)

int * afn_epsilon_fermeture(AFN A, int *R) {

  int q = A->dico['&'-ASCII_FIRST];
  int* res = malloc(sizeof(int));
  res[0] = -1;

  if (R == NULL)
    return res;

  //FAUT UNE PILE
  int i = 0;
  int j = 0;
  int size = 1;
  int temp;
  int* tmp;

  while (R[i] != -1) { //Pour chacune des valeurs de R

    push(R[i]); // On insère dans la pile le 1er élement à analyser

    size++;
    tmp = realloc(res,size*sizeof(int)); //on réaloue la place necessaire
    if (tmp == NULL) //une sécurité pour éviter un warning
      return NULL;
    res = tmp;
    res[size-1] = -1; //valeur de fin de liste
    res[size-2] = R[i]; //on met chacune des valeurs dans la liste

    while (top != NULL) { //tant que la pile n'est pas vide on continue de vérifier chacun de ses éléments

      temp = pop(); //on récupère la valeur au sommet de la pile

      printf("temp: %d\n",temp);

      if (A->delta[temp][q] != NULL) { //Si des valeurs de '&' à ajouter à la pile
        j = 0;
        while (A->delta[temp][q][j] != -1) { //tant que on a pas analysé toutes les valeurs du tableau
          size++;
          printf("size:%d\n",size);

          if (!AlreadyIn(res,A->delta[temp][q][j])) { //vérifier si on a déjà la valeur dans la liste
            tmp = realloc(res,size*sizeof(int)); //on réaloue la place necessaire
            if (tmp == NULL) //une sécurité pour éviter un warning
              return NULL;
            res = tmp;

            res[size-1] = -1; //valeur de fin de liste
            res[size-2] = A->delta[temp][q][j]; //on met chacune des valeurs dans la liste
            push(A->delta[temp][q][j]); //push au sommet de la pile //////
            printf("value: %d\n",A->delta[temp][q][j]);
          }
          j++;
        }
      }
    }
    i++;
  }
  return res;
}
/*
 * FUNCTION: afn_simuler
 * ---------------------
 * renvoie 1 si la chaine `s` est acceptée par l'AFN `A`, 0 sinon
 *
 * param:
 *        A - un AFN
 *        s - la chaine de caractères à analyser
 */

int afn_simuler(AFN A, char *s) {

  int q;
  int* listeEtats;
  int nbEtats = A->lenI;
  int size;

  if (s == NULL)
    return 0;

  listeEtats = malloc( nbEtats  * sizeof(int)); //On initie les valeurs à traiter aux états initiaux
  for (int k=0; k < nbEtats; k++)
    listeEtats[k] = A->I[k];

  printf("etats initiaux: %d,%d\n",listeEtats[0],listeEtats[1]);

  for (int i=0; i < strlen(s); i++) { //Pour chaque caractère du string

    printf("Pour le caractère : %c\n", s[i]);

    q = A->dico[s[i] - ASCII_FIRST];
    int* tmpListe = malloc(sizeof(int)); //On créé des variables temporaires
    int tmpNb = 0;

    for (int el = 0; el < nbEtats ; el++) { //pour chaque etat de la liste d'etats
      if (A->delta[listeEtats[el]][q] != NULL){ //on vérifie que le dit etat ne soit pas vide
        int size = 0;
        printf("  Pour l'état : %d\n", listeEtats[el]);
        while ( A->delta[listeEtats[el]][q][size] != -1 ) { // pour chaque etat d'etat d'etat
  
          tmpNb++;
  
          int* tmp = realloc(tmpListe, tmpNb * sizeof(int) ); //on réaloue la place necessaire
          if (tmp == NULL) //une sécurité pour éviter un warning
            return 0;
          tmpListe = tmp;
          printf("    Ajoutons la valeur : %d\n", A->delta[listeEtats[el]][q][size]); //On ajoute les valeurs à notre liste temporaire
          tmpListe[tmpNb-1] = A->delta[listeEtats[el]][q][size];
          size++;
  
        }
      }
    }

    //On met les valeurs temporaires dans leurs variables respectives
    nbEtats = tmpNb;
    free(listeEtats);

    int* listeEtats = malloc(nbEtats*sizeof(int));
    printf("-> De longeur: %d, nous avons: ",nbEtats);
    for (int k=0; k < nbEtats; k++){
      listeEtats[k] = tmpListe[k];
      printf("%d ",listeEtats[k]);
    }
    printf("\n");

    free(tmpListe); //On libère les variables temporaires pour reset
  }

  //Quand c'est fini, on a dans notre liste les états finaux à vérifier

  for (int r=0; r < A->lenF; r++){ //On parcourt la liste des états finaux et on renvoit 1 quand
    for (int y=0; y<nbEtats; y++){ //l'un des etats actuels et un etat final
      if (listeEtats[y] == A->F[r])  //etat final trouvé, retourner true       
        return 1;
    }
  }
  return 0;
}
