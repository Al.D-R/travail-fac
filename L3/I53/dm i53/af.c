#include <stdio.h>
#include "afd.h"
#include "afn.h"

int main(int argc, char *argv[]){
/*
  int Q=3,q0=0,nbFinals=2,listFinals[2]={1,3}, nbInitaux=2, listInitiaux[2]={0,1};
  char Sigma[]="abc";
*/
/*
  AFD  A;

  A = afd_finit("./file_afd.txt");

  A = afd_init(Q,q0,nbFinals,listFinals,Sigma);

  afd_ajouter_transition(A, 0,'a',2);
  afd_ajouter_transition(A, 0,'b',1);
  afd_ajouter_transition(A, 0,'c',0);
  afd_ajouter_transition(A, 1,'a',2);
  afd_ajouter_transition(A, 1,'b',0);
  afd_ajouter_transition(A, 1,'c',3);
  afd_ajouter_transition(A, 2,'a',3);
  afd_ajouter_transition(A, 2,'b',1);
  afd_ajouter_transition(A, 2,'c',3);
  afd_ajouter_transition(A, 3,'a',2);
  afd_ajouter_transition(A, 3,'b',1);
  afd_ajouter_transition(A, 3,'c',3);

  afd_print(A);

  printf("%d\n",afd_simuler(A,"babac"));

  afd_free(A);
  */
  /*
  int Q=3,nbFinals=2,listFinals[2]={1,3}, nbInitaux=2, listInitiaux[2]={0,1};
  char Sigma[]="abc";
  */

  AFN B;
  //B = afn_init(Q,nbInitaux,listInitiaux,nbFinals,listFinals,Sigma);
  B = afn_finit("./file_afn.txt");
  /*
  afn_ajouter_transition(B, 0,'a',2);
  afn_ajouter_transition(B, 0,'a',1);
  afn_ajouter_transition(B, 0,'a',0);
  afn_ajouter_transition(B, 1,'a',2);
  afn_ajouter_transition(B, 1,'b',0);
  afn_ajouter_transition(B, 1,'c',3);
  afn_ajouter_transition(B, 2,'a',3);
  afn_ajouter_transition(B, 2,'b',1);
  afn_ajouter_transition(B, 2,'c',3);
  afn_ajouter_transition(B, 3,'a',2);
  afn_ajouter_transition(B, 3,'b',1);
  afn_ajouter_transition(B, 3,'c',3);
  */
  afn_print(B);

  /*
  int p[3] = {0,7,-1};
  int* res = afn_epsilon_fermeture(B,p);
  int kek=0;
  while (res[kek] != -1) {
    printf("%d ",res[kek]);
    kek++;
  }
  printf("\n");
  */

  printf("works? %d\n",afn_simuler(B, "&&&"));
  printf("works? %d\n",afn_simuler(B, "&&b"));


  afn_free(B);

  return 0;
}
