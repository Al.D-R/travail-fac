* 1

E = [01][0-9]{2}|25[0-5]|2[0-4][0-9]
E = 25[0-5]|2[0-4][0-9]|1[0-9]{2}|0?[1-9]{2}|0?0?[0-9]

E.E.E.E

Ou

(25[0-5]|2[0-4][0-9]|1[0-9]{2}|0?[1-9]{2}|0?0?[0-9]\.){3}25[0-5]|2[0-4][0-9]|1[0-9]{2}|0?[1-9]{2}|0?0?[0-9]

Ou

(25[0-5]|2[0-4][0-9]|1[0-9]{2}|0?[1-9]{2}|0?0?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|0?[1-9]{2}|0?0?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|0?[1-9]{2}|0?0?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|0?[1-9]{2}|0?0?[0-9])
