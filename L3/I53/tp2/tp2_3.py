import sys
import os
import stat

# ex 1
# E  -> T E'
# E' -> Ou T E' | epsilon
# T  -> F T'
# T' -> Et F T' | epsilon
# F  -> Non F | B
# B  -> Vrai | Faux | (E)

stack = []
tokens = []
i = 0

def analex(s):
  l = []
  i = 0
  while i < len(s):
    c = s[i]
    if c == 'O' and s[i + 1] == 'u': 
      l.append("OU")
      i += 1
    elif c == 'E' and s[i + 1] == 't':
      l.append("ET")
      i += 1
    elif c == 'N' and s[i + 1] == 'o' and s[i + 2] == 'n':
      l.append("NON")
      i += 2
    elif c == 'V' and s[i + 1] == 'r' and s[i + 2] == 'a' and s[i + 3] == 'i':
      l.append("Vrai")
      i += 3
    elif c == 'F' and s[i + 1] == 'a' and s[i + 2] == 'u' and s[i + 3] == 'x':
      l.append("Faux")
      i += 3
    elif c == '(':
      l.append("LPar")
    elif c == ')':
      l.append("RPar")
    elif c == ' ':
      while i < len(s) and s[i] == ' ':
        i += 1
      i -= 1
    else:
      print("Analex error")
      exit(3)
    i += 1
  return l

def reconnaitre(lxm):
  global tokens, i
  if i < len(tokens):
    t= tokens[i]
    if lxm == "OU" and t == "OU": 
      i += 1
      return True
    elif lxm == "ET" and t == "ET":
      i += 1
      return True
    elif lxm == "NON" and t == "NON":
      i += 1
      return True
    elif lxm == "Vrai" and t == "Vrai":
      i += 1
      return True
    elif lxm == "Faux" and t == "Faux":
      i += 1
      return True
    elif lxm == 'LPar' and t == 'LPar':
      i += 1
      return True
    elif lxm == 'RPar' and t == 'RPar':
      i += 1
      return True
    else:
      return False

def expr():
  return terme() and reste()

def reste():
  global deepness
  if reconnaitre("OU"):
    res1 = terme()
    stack.append('OU')
    res2 = reste()
    return res1 and res2
  elif reconnaitre("RPar"):
    return True
  else:
    return True

def reste2():
  if reconnaitre("ET"):
    res1 = fact()
    stack.append("ET")
    res2 = reste2()
    return res1 and res2
  else:
    return True

def fact():
  if reconnaitre("NON"):
    stack.append("NON")
    return fact()
  else:
    return booleen()

def terme():
  return fact() and reste2()

def booleen():
  if reconnaitre("Vrai"):
    stack.append("Vrai")
    return True
  elif reconnaitre("Faux"):
    stack.append("Faux")
    return True
  elif reconnaitre("LPar"):
    return expr()
  else:
    return False

def prod():
  deepness = 1
  res = "#!/usr/bin/python\n"
  for s in stack:
    if s == "OU":
      deepness -= 2
      res += ("t" + str(deepness) + " = t" + str(deepness) + " or t" + str(deepness + 1) + "\n")
      deepness += 1
    elif s == "ET":
      deepness -= 2
      res += ("t" + str(deepness) + " = t" + str(deepness) + " and t" + str(deepness + 1) + "\n")
      deepness += 1
    elif s == "NON":
      deepness -= 1
      res += ("t" + str(deepness) + " = t" + str(deepness) + " not t" + str(deepness + 1) + "\n")
      deepness += 1
    elif s == "Vrai":
      res += ("t" + str(deepness) + " = True\n")
      deepness += 1
    else:
      res += ("t" + str(deepness) + " = False\n")
      deepness += 1
  res += "print(t1)\n"
  return res


if __name__ == "__main__":
  if len(sys.argv) < 2:
    print("Veuillez specifier un argument")
    exit(1)

  tokens = analex(sys.argv[1])
  print("TOKENS", tokens)
  if expr():
    f = open("/tmp/a.out", "w")
    f.write(prod())
    os.chmod("/tmp/a.out", stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IXGRP)
  else:
    print("Erreur au caractere ", tokens[i-1])

    
