import sys

# ex 1
# Expr -> Terme
#       | Terme + Nb
#       | Terme - Nb
# Terme -> Nb
# Nb -> 0 | ... | 9

stack = []
tokens = [("Nb", 3), ("Plus", '+'), ("Nb", 6), ("Minus", '-'), ("Nb", 2)]
i = 0

def reconnaitre(lxm):
	global tokens, i
	if i < len(tokens):
		t= list(tokens[i])[0]
		v= list(tokens[i])[1]
		if lxm == "Plus" and t == "Plus" and v == '+': 
			i += 1
			return True
		elif lxm == "Minus" and t == "Minus" and v == '-':
			i += 1
			return True
		elif lxm == "Nb" and t == "Nb" and str(v).isdigit():
			i += 1
			return True
		else:
			return False

def expr():
	return terme() and reste()

def reste():
	if reconnaitre("Plus"):
		res1 = terme()
		stack.append('+')
		res2 = reste()
		return res1 and res2
	elif reconnaitre("Minus"):
		res1 = terme()
		stack.append('-')
		res2 = reste()
		return res1 and res2
	else:
		return True

def terme():
	if reconnaitre("Nb"):
		stack.append(list(tokens[i-1])[1])
		return True


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print("Veuillez specifier un argument")
		exit(1)
	if expr():
		print("Chaine valide")
		print(stack)
	else:
		print("Erreur au caractere: ", list(tokens[i])[1])

		
