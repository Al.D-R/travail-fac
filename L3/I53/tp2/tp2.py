import sys
import os
import stat

# ex 1
# Expr -> Terme
#       | Expr + Terme
#	| Expr - Terme
# Terme -> Fact
#       | Terme * Fact
# Fact -> Nb
#       | (Expr)
# Nb -> 0 | ... | 9

stack = []
tokens = []
i = 0

def analex(s):
	l = []
	i = 0
	while i < len(s):
		c = s[i]
		if c == '+': l.append(("Op", '+'))
		elif c == '-': l.append(("Op", '-'))
		elif c == '*': l.append(("Op", '*'))
		elif c == '/': l.append(("Op", '/'))
		elif c == '(': l.append(("LPar", '('))
		elif c == ')': l.append(("RPar", ')'))
		elif c == ' ':
			while i < len(s) and s[i] == ' ':
				i += 1
			i -= 1
		elif c.isdigit():
			ss = ""
			while i < len(s) and s[i].isdigit():
				ss += s[i]
				i += 1
			i -= 1
			l.append(("Nb", int(ss)))
		i += 1
	return l

def reconnaitre(lxm):
	global tokens, i
	if i < len(tokens):
		t= list(tokens[i])[0]
		v= list(tokens[i])[1]
		if lxm == "Plus" and t == "Op" and v == '+': 
			i += 1
			return True
		elif lxm == "Minus" and t == "Op" and v == '-':
			i += 1
			return True
		elif lxm == "Times" and t == "Op" and v == '*':
			i += 1
			return True
		elif lxm == "Div" and t == "Op" and v == '/':
			i += 1
			return True
		elif lxm == "Nb" and t == "Nb" and str(v).isdigit():
			i += 1
			return True
		elif lxm == 'LPar' and t == "LPar" and v == '(':
			i += 1
			return True
		elif lxm == 'RPar' and t == "RPar" and v == ')':
			i += 1
			return True
		else:
			return False

def expr():
	return terme() and reste()

def reste():
	global deepness
	if reconnaitre("Plus"):
		res1 = terme()
		stack.append('+')
		res2 = reste()
		return res1 and res2
	elif reconnaitre("Minus"):
		res1 = terme()
		stack.append('-')
		res2 = reste()
		return res1 and res2
	elif reconnaitre("RPar"):
		return True
	else:
		return True

def reste2():
	if reconnaitre("Times"):
		res1 = terme()
		stack.append('*')
		res2 = reste2()
		return res1 and res2
	elif reconnaitre("Div"):
		res1 = terme()
		stack.append('/')
		res2 = reste2()
		return res1 and res2
	else:
		return True

def fact():
	if reconnaitre("Nb"):
		stack.append(list(tokens[i-1])[1])
		return True
	elif reconnaitre("LPar"):
		return expr()


def terme():
	return fact() and reste2()

def prod():
	deepness = 1
	res = "#!/usr/bin/python\n"
	for s in stack:
		if s == "+":
			deepness -= 2
			res += ("t" + str(deepness) + " = t" + str(deepness) + " + t" + str(deepness + 1) + "\n")
			deepness += 1
		elif s == "-":
			deepness -= 2
			res += ("t" + str(deepness) + " = t" + str(deepness) + " - t" + str(deepness + 1) + "\n")
			deepness += 1
		elif s == "*":
			deepness -= 2
			res += ("t" + str(deepness) + " = t" + str(deepness) + " * t" + str(deepness + 1) + "\n")
			deepness += 1
		elif s == "/":
			deepness -= 2
			res += ("t" + str(deepness) + " = t" + str(deepness) + " / t" + str(deepness + 1) + "\n")
			deepness += 1
		else:
			res += ("t" + str(deepness) + " = " + str(s) + "\n")
			deepness += 1
	res += "print(t1)\n"
	return res


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print("Veuillez specifier un argument")
		exit(1)
	tokens = analex(sys.argv[1])
	print("TOKENS", tokens)
	if expr():
		
		f = open("/tmp/a.out", "w")
		f.write(prod())
		os.chmod("/tmp/a.out", stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IXGRP)
	else:
		print("Erreur au caractere: ", list(tokens[i-1])[1])

		
