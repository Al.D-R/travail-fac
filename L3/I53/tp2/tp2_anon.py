import sys
from scanner import scanner

def reconnaitre(lexeme):
    global liste,p
    if (p>=len(liste)):
        return False
    if (lexeme==liste[p][0]):
        p+=1
        return True
    else:
        print("Le caractère fautif est:",liste[p][1],", d'indice:",p)
        return False

def reste():
    if reconnaitre("OP"):
        return (terme() and reste())
    else:
        return True

def terme():
    return reconnaitre("NB")

def expr():
    return (terme() and reste())


def Parser(s):
    global liste
    #liste = scanner(s)
    liste=[("NB",2),("OP","+"),("NB", 3)]
    global p
    p = 0
    print(expr())

if __name__=="__main__":
    if len(sys.argv)>1:
        print (Parser(sys.argv[1]))
