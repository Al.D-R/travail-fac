AFN afn_char(char c, char* sigma) {
  int Q = 1, lenI = 1, lenF = 1;
  int I[1] = { 0 };
  int F[1] = { 1 };
  Anf A = afn_init(Q, lenI, I, lenF, F, sigma);
  afn_ajouter_transition(A, 0, c, 1);
  return A;
}

AFN afn_union(AFN A, AFN B) {
  int Q = A->Q + B->Q + 2, lenI = 1, lenF = A->lenF + B->lenF, i, j;
  int I[1] = { 0 };
  int *F = malloc(sizeof(int) * lenF);
  // decallage = 1
  for (i = 0; i < A->lenF; i++) {
    F[j] = A->F[i] + decallage;
  }
  // B->F, decallage = A->Q + 2

  AFN C = afn_init(...);
  for (transition: A->Delta)
    afn_ajouter_transition(C, q1 + decallage, c, q2 + decallage);
  for (transition: B->Delta)
    afn_ajouter_transition(C, q1 + decallage, c, q2 + decallage);
  for (i = 0; i < A->lenI, i++)
    afn_ajouter_transition(C, 0, '&', A->Delta[q1][i] + decallage);
  for (i = 0; i < B->lenI, i++)
    afn_ajouter_transition(C, 0, '&', B->Delta[q1][i] + decallage);

  return C;
}
