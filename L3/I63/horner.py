
def horner(p, x):
    n = len(p)
    res = p[n - 1]
    for k in range(n - 2, -1, -1):
        res = res * x + p[k]
    return res


def evalpol(p, a, b, step):
    x = a
    while x <= b:
        res = horner(p, x)
        print(x, res)
        x += step

p = [5, 0, 5, 2]
evalpol(p, 0, 20, 1)
