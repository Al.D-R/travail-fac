import tkinter as tk
from libinfo import *

def est_point_haut(poly, k):
    y = poly["pts"][k][1]
    y1 = poly["pts"][(k-1) % poly["nb_pts"]][1]
    y2 = poly["pts"][(k+1) % poly["nb_pts"]][1]
    return y < y1 and y < y2 # Repere indirecte pour canvas

def TA(poly):
    ta = []
    for i in range(poly["nb_pts"]):
        if not est_point_haut(poly, i):
            ta.append(poly["pts"][i])
    return ta

# Initialisation de la fenetre
root = tk.Tk()

canv = tk.Canvas(root, width = 900, height = He)
canv.pack(fill="both", expand=1)

draw_viewport(canv)

# Dessin du polygone
poly = {
    "nb_pts": 3,
    "pts": coord_list_convert([(25, 25), (50, 50), (25, 75)]),
    "color": "red"
}

draw_polygon(canv, poly)

print(TA(poly))

root.mainloop()
