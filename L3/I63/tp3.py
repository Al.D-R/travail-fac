import tkinter as tk

def maplist(f, l):
  """Un map sans autisme"""
  res = []
  for x in l:
    res.append(f(x))
  return res

obj_counter = 0
obj_coords = {}

# Hauteur d'ecran
He = 600

# Taille et position de la window
x_w = -2
y_w = 0
dx_w = 4
dy_w = 4

# Taille de la viewport
dx_v = 600
dy_v = 400
width = dx_v
height = dy_v

# Coordonnees de la viewport dans la window
x_vp = 50
# repere directe
_y_vp = 50
# repere indirecte
y_vp = He - _y_vp - dy_v

def coord_convert(_x, _y):
  """Convertit les coordonnées """
  x = _x * dx_v / dx_w + (x_vp - x_w*dx_v/dx_w)
  y = -_y * dy_v / dy_w + (-_y_vp + y_w*dy_v/dy_w + He)
  return (x, y)

def coord_list_convert(l):
  """Applique la conversion a une liste de points"""
  return maplist(lambda t: coord_convert(t[0], t[1]), l)

def draw_viewport(canv):
  """Dessine le rectangle correspondant a la viewport en effacant le precedent"""
  canv.delete("viewport")
  canv.create_rectangle(x_vp, y_vp, x_vp + width, y_vp + height, tags="viewport", fill="white")
  canv.tag_bind("viewport", sequence="<B1-Motion>", func = lambda e: move_viewport(e, canv))

def add_point(canv, x, y):
  """Ajoute un point de coordonnees (x, y) (repere indirecte) au canvas"""
  global obj_counter; global obj_coords
  canv.create_oval(x, y, x+5, y+5, tags="obj" + str(obj_counter))
  obj_coords[obj_counter] = (x, y)
  obj_counter += 1

def add_point_list(canv, l):
  """Ajoute les points de la liste l dans au canvas"""
  for p in l:
    add_point(canv, p[0], p[1])

def move_viewport(evt, canv):
  """Deplace le rectangle correspondant a la viewport"""
  global x_vp; global y_vp
  x_vp = evt.x
  y_vp = evt.y
  canv.moveto("viewport", x=evt.x, y=evt.y)


# Initialisation de la fenetre
root = tk.Tk()

canv = tk.Canvas(root, width = 900, height = He)
canv.pack(fill="both", expand=1)

draw_viewport(canv)

# f = open("/tmp/test.txt", "r")
# l = coords_of_file(f)
# print("Coords: ", l)
# l = coord_list_convert(l)
# print("Coords converties: ", l)
# add_point_list(canv, l)

p = coord_convert(1, 1)
add_point(canv, p[0], p[1])


root.mainloop()
