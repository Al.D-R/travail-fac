import tkinter as tk

def maplist(f, l):
  """Un map sans autisme"""
  res = []
  for x in l:
    res.append(f(x))
  return res

obj_counter = 0
obj_coords = {}

# Hauteur d'ecran
He = 600

# Taille et position de la window
x_w = 0
y_w = 0
dx_w = 100
dy_w = 100

# Taille de la viewport
dx_v = 600
dy_v = 400
width = dx_v
height = dy_v

# Coordonnees de la viewport dans la window
x_vp = 300
# repere directe
_y_vp = 300
# repere indirecte
y_vp = He - _y_vp - dy_v

def coord_convert(_x, _y):
  """Convertit les coordonnées """
  #x = _x * dx_v / dx_w + (x_vp - x_w*dx_v/dx_w)
  #y = -_y * dy_v / dy_w + (-_y_vp + y_w*dy_v/dy_w + He)
  x = _x * dx_v / dx_w + (x_vp - x_w*dx_v/dx_w)
  y = _y * dy_v / dy_w + (_y_vp - y_w*dy_v/dy_w)
  return (x, y)

def coord_list_convert(l):
  """Applique la conversion a une liste de points"""
  return maplist(lambda t: coord_convert(t[0], t[1]), l)

def draw_viewport(canv):
  """Dessine le rectangle correspondant a la viewport en effacant le precedent"""
  canv.delete("viewport")
  canv.create_rectangle(x_vp, y_vp, x_vp + width, y_vp + height, tags="viewport", fill="white")
  canv.tag_bind("viewport", sequence="<B1-Motion>", func = lambda e: move_viewport(e, canv))

def add_point(canv, x, y):
  """Ajoute un point de coordonnees (x, y) (repere indirecte) au canvas"""
  global obj_counter; global obj_coords
  canv.create_oval(x-1, y-1, x, y, tags="obj" + str(obj_counter))
  obj_coords[obj_counter] = (x, y)
  obj_counter += 1

def add_point_list(canv, l):
  """Ajoute les points de la liste l dans au canvas"""
  for p in l:
    add_point(canv, p[0], p[1])

def move_viewport(evt, canv):
  """Deplace le rectangle correspondant a la viewport"""
  global x_vp; global y_vp
  x_vp = evt.x
  y_vp = evt.y
  canv.moveto("viewport", x=evt.x, y=evt.y)

def segment_naif(canv, p1, p2):
  """Algo de tracé de segments tout pourri"""
  vec = (p2[0] - p1[0], p2[1] - p1[1])
  i = p1[0]
  _x = p1[0] + i * vec[0]
  while _x <= p2[0]:
    _x = p1[0] + i * vec[0]
    _y = p1[1] + i * vec[1]
    (x, y) = coord_convert(_x, _y)
    add_point(canv, x, y)
    i += 0.01

def bresenham(canv, p1, p2):
  """Algo minimal de Bresenham (1er octant et 1er point à l'origine)"""
  (a, b) = coord_convert(p1[0], p1[1])
  (c, d) = coord_convert(p2[0], p2[1])
  # Mise a l'origine
  c -= a
  d -= b
  a = 0
  b = 0
  dx = c - a
  dy = d - b
  # Variable de decision
  x = 0
  y = 0
  if dx != 0:
    if dx > 0:
      if dy != 0:
        # Vecteur dans le 1er quadran
        if dy > 0:
          # 1er octant
          if dx >= dy:
            dec = dx - 2 * dy
            while x <= dx:
              _y = -y + y_vp + dy_v
              _x = x + x_vp
              add_point(canv, _x, _y)
              if dec < 0:
                dec += 2 * dx
                y += 1
              dec -= 2 * dy
              x += 1
          # 2e octant
          else:
            dec = dy - 2 * dx
            while y <= dy:
              _y = -y + y_vp + dy_v
              _x = x + x_vp
              add_point(canv, _x, _y)
              if dec < 0:
                dec += 2 * dy
                x += 1
              dec -= 2 * dx
              y += 1
        # 4e cadran
        else:
          # 8e octant
          if dy >= -dx:
            dec = dx + 2 * dy
            while x <= dx:
              _y = -y + y_vp + dy_v
              _x = x + x_vp
              add_point(canv, _x, _y)
              if dec < 0:
                dec += 2 * dx
                y -= 1
              dec += 2 * dy
              x += 1
          # 7e octant
          else:
            dec = dy + 2 * dx
            while y >= dy:
              _y = -y + y_vp + dy_v
              _x = x + x_vp
              add_point(canv, _x, _y)
              if dec > 0:
                dec += 2 * dy
                x += 1
              dec += 2 * dx
              y -= 1
    else:
      if dy != 0:
        # Vecteur dans le 2nd quadran
        if dy > 0:
          # 4e octant
          if -dx >= dy:
            dec = dx + 2 * dy
            while x >= dx:
              _y = -y + y_vp + dy_v
              _x = x + x_vp
              add_point(canv, _x, _y)
              if dec > 0:
                dec += 2 * dx
                y += 1
              dec += 2 * dy
              x -= 1
          # 3e octant
          else:
            dec = dy + 2 * dx
            while y <= dy:
              _y = -y + y_vp + dy_v
              _x = x + x_vp
              add_point(canv, _x, _y)
              if dec < 0:
                dec += 2 * dy
                x -= 1
              dec += 2 * dx
              y += 1

        # 3e quadran
        else:
          # 5e octant
          if dx <= dy:
            dec = dx - 2 * dy
            while x >= dx:
              _y = -y + y_vp + dy_v
              _x = x + x_vp
              add_point(canv, _x, _y)
              if dec > 0:
                dec += 2 * dx
                y -= 1
              dec -= 2 * dy
              x -= 1
          # 6e octant
          else:
            dec = dy + 2 * dx
            while y >= dy:
              _y = -y + y_vp + dy_v
              _x = x + x_vp
              add_point(canv, _x, _y)
              if dec < 0:
                dec -= 2 * dy
                x -= 1
              dec += 2 * dx
              y -= 1


# Initialisation de la fenetre
root = tk.Tk()

canv = tk.Canvas(root, width = 900, height = He)
canv.pack(fill="both", expand=1)

draw_viewport(canv)

# Dessin des points
p1 = (10, 10)
p2 = (60, 20)
#segment_naif(canv, p1, p2)
bresenham(canv, p1, p2)

root.mainloop()
