\babel@toc {french}{}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}%
\contentsline {section}{\numberline {2}Définition}{1}{section.2}%
\contentsline {section}{\numberline {3}Propriétés}{2}{section.3}%
\contentsline {section}{\numberline {4}Polynômes particulier}{2}{section.4}%
\contentsline {subsection}{\numberline {4.1}Polynômes caractéristiques}{2}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Polynômes de Bernstein}{2}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Polynômes interpolateur de Lagrange}{2}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Polynômes de Tchebychev}{3}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Polynômes d'Hermite}{3}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Polynômes d'Hermite}{4}{subsection.4.6}%
