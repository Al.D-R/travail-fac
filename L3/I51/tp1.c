void dessinner(char *nom, Graphe g) {
  int i,j;
  FILE *dst = NULL;
  char name[128], cmd[128];

  sprintf(name, "/tmp/%s.dot", nom);
  dst = fopen(name, "w");
  fprintf(dst, "graph %s {\n}", nom);

  for (i = 0; i < g.ordre; i++)
    for (j = 0; j < g.ordre; j++)
      if (g.mat[i][j])
        fprintf(dst, "%d--%d\n", i, j);

  fprintf(dst, "}\n");
  sprintf(cmd, "dot -Tpng %s -o %s.png", name, nom);
  system(cmd);
  fclose(dst);
}
