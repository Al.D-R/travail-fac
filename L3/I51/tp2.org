* Theme aborde
Connexite

* Ex

Construction d'un graphe aleatoire. On a un parametre p.
On decide de l'adjacence de 2 sommets avec la probabilite p / (n-1).

#+BEGIN_SRC c
  Graphe randomGraphe(int n, float p) {
    Graphe g = creerGraphe(n);
    float proba = p / (n - 1);
    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        if (rand () < proba) {
          g.mat[i][j] = 1;
          g.mat[j][i] = 1;
        }
    return g;
  }
#+END_SRC
