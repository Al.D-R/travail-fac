* 1
Ecrire une fonction grante(graphe g) qui retourne la taille de la + grande composante connexe.

#+BEGIN_SRC c

  int PPR2(int s, Graphe g, int *cpt) {
    int r;
    int kept = *cpt + 1;
    chr[s] = 1;
    for (r = 0; r < g.nbs; r++)
      if (g.mat[s][r] && !chr[r])
        cpt = PPR2(s, g, cpt) + 1;
    return cpt
      }

  int grante(Graphe g) {
    int maxi = 1;
    g.lr = calloc(g.nbs, sizeof(uchar));
    for (int s = 0; s <=g.nbs; s++) {
      if (ch[s] == 0) {
        int cpt = 0;
        cpt == PPR2(s, g, &cpt);
        if (cpt > maxi)
          maxi = cpt;
      }
    }
    free(g.lr);
    return res;
  }
#+END_SRC

* 2
Modifier cette fonction pour retourner une description des orbites:
- le representant
- La taille de la composante

#+BEGIN_SRC c

  typedef struct Paire {
    int sommet;
    int taille;
    struct Paire* suivant;
  } Paire;
  typedef Paire* LPaire;

  typedef struct {
    int sommet;
    int taille
  } Info;

  Info* geante() {
    Info* res;
    int p = 0;
    res = calloc(g.nbs = 1, sizeof(uchar));

    res[p].sommet = s;
    res[p].taille = taille;
    p++;
  }

  LPaire grante(Graphe g) {
    LPaire res = NULL;
    int maxi = 1;
    g.lr = calloc(g.nbs, sizeof(uchar));
    for (int s = 0; s <=g.nbs; s++) {
      if (ch[s] == 0) {
        int cpt = 0;
        cpt == PPR2(s, g, &cpt);
        if (cpt > maxi)
          maxi = cpt;
      }
    }
    free(g.lr);
    return LPaire;
  }
#+END_SRC

* 3
Ecrire un algorithme Eulerien(s: sommet, g: graphe) qui affiche un cycle eulerien.
Sachant qu'il est eulerien.

Eulerien(s: sommet, g: graphe)
DEBUT
  p <- Promenade(0, g)
  Pour chaque s: sommet de p
    Si deg(s) > 0
      Eulerien(s, g)
    Sinon
      Afficher(s)
FIN

Cet algo a une complexite de O(n^2) par liste d'adjacence et lineaire en matrice d'adjacence.

#+BEGIN_SRC c
  pile promenade(sommet s, graphe g) {
    pile res = NULL;
    empiler(s, res);
    while(deg(s, g) > 0) {
      sommet t = voisin(s, g);
      g.mat[s][t] = g.mat[t][s] = 0;
      empiler(t, res);
      s = t;
    }
  }
#+END_SRC




