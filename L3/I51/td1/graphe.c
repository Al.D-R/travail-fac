#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {

  int ordre;
  char **mat;

} Graphe;

char** str_split(char* a_str, const char a_delim)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}

Graphe* init_graphe(int n) {
  Graphe* g = malloc(sizeof(Graphe));
  g->ordre = n;
  g->mat = malloc(n * sizeof(char*));
  for (int i = 0; i < n; i++)
    g->mat[i] = malloc(n * sizeof(char));
  return g;
}

void free_graphe(Graphe* g) {
  for (int i = 0; i < g->ordre; i++)
    free(g->mat[i]);
  free(g->mat);
  free(g);
}

Graphe* lire_graphe(char* nfile) {
  FILE* file = NULL;
  file = fopen(nfile, "r");
  if (!file) {
    fprintf(stderr, "Erreur ouverture fichier");
    exit(1);
  }
  char* s;
  while (!feof(file)) {
    fgets(s, 64, file);
  }
}

int main(void) {

  char *s = strsplit1("coucou=bruh", '=');
  printf("%s\n", s);

  return 0;
}
