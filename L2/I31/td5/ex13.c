#include <stdio.h>

typedef struct  { float x, y; } Point;
typedef struct  { Point origin; float width, height; } Rectangle;

void printPoint(Point p) {
  printf("(%f, %f)", p.x, p.y);
}
void printRect(Rectangle r) {
  printf("[");
  printPoint(r.origin);
  printf(", width %f, height %f]", r.width, r.height);
}

int contientPoint(Rectangle r, Point p) {
  int x = p.x > r.origin.x && p.x < r.width;
  int y = p.y > r.origin.y && p.x < r.height;
  return x && y;
}

int main(void) {

  Point p = {0, 0};
  Rectangle r = { p, 5, 3};
  Point p2 = { 6, 2};
  printf("%d\n", contientPoint(r, p2));


  return 0;
}
