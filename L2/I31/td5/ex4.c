#include <stdio.h>

void aux(int* tab, unsigned int n, unsigned int i) {
  if (i < n) {
    printf("%d\n", tab[i]);
    aux(tab, n, i + 1);
  }
}
void printTab(int* tab, unsigned int n) {
  aux(tab, n, 0);
}

int main(void) {

  int tab[5] = {12, 45, 78, 98, 32};
  printTab(tab, 5);

  return 0;
}
