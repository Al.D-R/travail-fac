#include <stdio.h>

unsigned int fact(unsigned int n) {
  if (n == 0) return 1;
  return n * fact(n - 1);
}

int main(void) {

  int n = fact(5);
  printf("%d\n", n);

  return 0;
}
