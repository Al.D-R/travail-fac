#include <stdio.h>
#include <stdlib.h>

double moyenne(int taille, char** tab) {
	double moy = 0.0;
	for (int i = 1; i < taille; i++) {
		moy += atof(tab[i]);
	}
	moy = moy / (taille - 1);
	return moy;
}

double variance(int taille, char** tab) {
	double moy = moyenne(taille, tab);
	double s = 0.0;
	for (int i = 1; i < taille; i++) {
		s += (atof(tab[i]) - moy) * (atof(tab[i]) - moy);
	}
	s /= (taille - 1);
	return s;
}

int main(int argc, char** argv) {
	double moy = moyenne(argc, argv);
	double var = variance(argc, argv);
	printf("Moyenne = %lf, Variance = %lf\n\n", moy, var);

	return 0;
}
