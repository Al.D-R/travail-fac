#include <stdio.h>

int main(void) {

	int n;
	int i = 2;
	scanf("%d", &n);
	while (i * i <= n)
		i++;
	printf("Le plus petit carré supérieur à %d est: %d\n", n, i*i);

	return 0;
}
