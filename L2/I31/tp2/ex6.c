#include <stdio.h>
#include <stdlib.h>

void printcoef(int n) {

	for (int k = 0; k < n; k++) {
		printf("(%d %d) x^(%d - %d) + ", n, k, n, k);
	}
	printf("(%d %d) x^(%d - %d)\n", n, n, n, n);

}

int main(int argc, char** argv) {

	int n;
	scanf("%d", &n);
	printcoef(n);

	return 0;
}
