#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int int_max(int t[], unsigned int n) {
  if (n == 0) return INT_MIN;
  int maximmum = t[0];
  for (int i = 0; i < n; i++)
    if (t[i] > maximmum)
      maximmum = t[i];
  return maximmum;
}
int main(void) {

  int tab[0] = {};
  int m = int_max(tab, 0);
  printf("%d\n", m);

  return 0;
}
