#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

int pgcd(int a, int b) {
  if (b == 0) return a;
  return pgcd(b, a % b);
}

int ppcm(int a, int b) {
  if (a == 0 || b == 0) return 0;
  int res = a > b ? a : b;
  while ((res % a != 0) || (res % b != 0)) res++;
  return res;
}

int fac(int n) {
  if (n < 0) {
    fprintf(stderr, "Erreur: %d est négatif\n", n);
    return -1;
  }
  int res = 1;
  while (n > 0) res *= n--;
  return res;
}

float puissance(float x, int n) {
  if (n < 0) {
    fprintf(stderr, "Erreur: %d est négatif\n", n);
    return -1;
  }
  float res = 1;
  while (n > 0) {
    res *= x;
    n--;
  }

}

char conversion_caracter(char c) {
  if (! isalpha(c)) {
    fprintf(stderr, "%c n'est pas une lettre\n", c);
    return -1;
  }
  if (islower(c)) return toupper(c);
  if (isupper(c)) return tolower(c);
}


int main() {

  char c = '\0';
  scanf("%c", &c);
  c = conversion_caracter(c);
  if (c == -1) return -1;
  printf("%c\n", c);
  
  return 0;
}
