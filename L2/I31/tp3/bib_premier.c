
#include "bib_premier.h"

int est_premier(unsigned int n) {
  for (int i = 2; i < n; i++)
    if (n % i == 0) return 0;
  return 1;
}

void premiers_premier(unsigned int n) {
  int nb = 1;
  int i = 2;
  while (nb <= n) {
    if (est_premier(i)) {
      printf("%d ", i);
      nb++;
    }
    i++;
  }
  printf("\n");
}

