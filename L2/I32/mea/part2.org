* Dico
** 1 nomResp
** 2 numIntervention
** 3 date
** 4 nomTech
** 5 temps
** 6 nomClient
** 7 adresseClient
** 8 numClient
** 9 distance
** 10 quantite
** 11 refPiece
** 12 prix
** 13 nomPiece
** 14 numFacture
** 15 dateEmission

 
* Matrice
|    | 2 | 8 | 11 | 14 | 2 + 11 |
|----+---+---+----+----+--------|
|  1 | x |   |    |    |        |
|  2 | x |   |    |    |        |
|  3 | x |   |    |    |        |
|  4 | x |   |    |    |        |
|  5 | x |   |    |    |        |
|  6 | x | x |    | x  |        |
|  7 | x | x |    | x  |        |
|  8 | x | x |    | x  |        |
|  9 | x | x |    | x  |        |
| 10 |   |   |    |    | x      |
| 11 |   |   | x  |    |        |
| 12 |   |   | x  |    |        |
| 13 |   |   | x  |    |        |
| 14 | x |   |    | x  |        |
| 15 | x |   |    | x  |        |
|----+---+---+----+----+--------|


* MEA
|----------------------+----------------------+----------------------|
|                      |                      | *client*             |
|                      |                      | ~numClient~          |
|                      |                      | nomClient            |
|                      |                      | adresseClient        |
|                      |                      | distance             |
|----------------------+----------------------+----------------------|
|                      |                      | 1, n <concerne> 1, 1 |
|----------------------+----------------------+----------------------|
| *fiche_intervention* |                      | *facture*            |
| ~numFiche~           | 1, 1 <concerne> 1, n | ~numFacture~         |
| nomResp              |                      | dateEmission         |
| nomTech              |                      |                      |
| date                 |                      |                      |
| temps                |                      |                      |
|----------------------+----------------------+----------------------|
| 0, n <remplace> 0, n |                      |                      |
|----------------------+----------------------+----------------------|
| *piece*              |                      |                      |
| ~refPiece~           |                      |                      |
| nomPiece             |                      |                      |
| prix                 |                      |                      |
|----------------------+----------------------+----------------------|


* Relationnel
fiche_intervention(_numFiche_, nomResp, nomTech, date, temps, #numFacture)
client(_numCLient_, nomClient, addresseClient, distance)
piece(_refPiece_, nomPiece, prix)
facture(_numFacture_, dateEmission, #numClient)
remplacement_pieces(_#numFiche, #refPiece_, quantite)

* Requetes
#+BEGIN_SRC sql
  SELECT count(numFiche), numClient FROM fiche_intervention NATURAL JOIN facture NATURAL JOIN client GROUP BY numClient;
#+END_SRC
#+BEGIN_SRC sql
  SELECT count(refPiece), quantite, numFiche FROM piece NATURAL JOIN remplacement NATURAL JOIN fiche_intervention GROUP BY numFiche;
#+END_SRC
#+BEGIN_SRC sql
  SELECT nomClient, adressClient FROM facture NATURAL JOIN client;
#+END_SRC
#+BEGIN_SRC sql
  SELECT numFiche FROM fiche_intervention fi, facture f WHERE fi.numFacture <> f.numFacture
#+END_SRC
