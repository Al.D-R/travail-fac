1) SELECT nom, age FROM participant;
2) SELECT avg(age) FROM participant;
3) SELECT count(id_participant), sexe FROM participant GROUP BY sexe;
4) SELECT danse, count(danse) FROM cours GROUP BY danse;
5) SELECT id_cours FROM inscription NATURAL JOIN cours;
OU
   SELECT id_cours FROM cours WHERE id_cours IN (SELECT id_cours FROM inscription);

6) (SELECT id_cours FROM cours) EXCEPT (SELECT id_cours FROM inscription);
OU
SELECT id_cours FROM cours WHERE id_cours NOT IN (SELECT id_cours FROM inscription);

7) SELECT nom FROM intervenant NATURAL JOIN atelier WHERE id_cours NOT IN (SELECT id_cours FROM inscription);
8) Select distinct P.nom,string_agg(distinct P.danse,',') As "Danses pratiquées"
FROM ( (Select p.nom As nom,c.danse As danse From Participant p,inscription i, cours c WHERE .id_participant = i.id_participant AND i.id_cours = c.id_cours AND (c.danse = 'Rock' Or c.danse = 'Salsa'))) P Group By P.nom Having count(distinct P.danse) = 2;

9) SELECT c.danse,count(i.id_intervenant) AS "Nombre d'intervenants" FROM cours c, atelier a,intervenant i WHERE c.id_cours = a.id_cours AND i.id_intervenant = a.id_intervenant GROUP BY c.danse;
10) SELECT nom, danse FROM cours NATURAL JOIN atelier NATURAL JOIN intervenant GROUP BY danse, nom;
11) SELECT * FROM participant ORDER BY age DESC LIMIT 1;
12) SELECT danse FROM cours NATURAL JOIN (SELECT id_cours, count(id_cours) FROM inscription GROUP BY id_cours ORDER BY count(id_cours) DESC LIMIT 1) as foo;
13) SELECT id_cours FROM atelier GROUP BY id_cours HAVING count(id_intervenant) = 1;
14) SELECT c.id_cours FROM cours c,atelier a,intervenant i WHERE c.id_cours = a.id_cours AND a.id_intervenant = i.id_intervenant Group BY c.id_cours,c.danse HAVING count(i.id_intervenant) >= ALL(Select count(a.id_intervenant) From atelier a Group BY a.id_cours) Order BY c.danse ASC;
15) select id_cours, count(*) from cours NATURAL JOIN inscription NATURAL JOIN participant GROUP BY id_cours;
16) select count(id_cours) from intervenant NATURAL JOIN atelier WHERE nom = 'Gilles';
17) SELECT id_participant, nom FROM participant NATURAL JOIN inscription NATURAL JOIN cours WHERE danse = 'Salsa' GROUP BY nom, id_participant HAVING count(id_participant) = (SELECT count(DISTINCT(id_cours)) FROM cours c WHERE danse = 'Salsa');
18) SELECT id_participant, nom FROM participant NATURAL JOIN inscription NATURAL JOIN cours WHERE id_participant = id_participant AND id_cours = id_cours GROUP BY id_participant, nom HAVING count(distinct danse) = 2 ORDER BY nom ASC;
19) select id_cours, danse from cours NATURAL JOIN atelier NATURAL JOIN intervenant WHERE nom = 'Amandine';
20) SELECT c.id_cours,count(p.id_participant) As "Nb Participants" FROM cours c,intervenant i,atelier a,inscription j,participant p WHERE a.id_cours = j.id_cours AND i.id_intervenant = a.id_intervenant AND p.id_participant = j.id_participant AND j.id_cours = c.id_cours AND i.nom = 'Amandine' GROUP BY c.id_cours;
20 version 2) SELECT c.id_cours,count(p.id_participant) As "Nb Participants" 
FROM intervenant i,atelier a, cours c NATURAL JOIN inscription j NATURAL JOIN participant p 
WHERE a.id_cours = j.id_cours AND i.id_intervenant = a.id_intervenant AND i.nom = 'Amandine' GROUP BY c.id_cours;
21) (SELECT id_cours, danse FROM cours NATURAL JOIN participant NATURAL JOIN inscription WHERE nom = 'Melanie') INTERSECT (SELECT id_cours, danse FROM cours NATURAL JOIN participant NATURAL JOIN inscription WHERE nom = 'Henri');
22) select id_participant, nom from participant NATURAL JOIN inscription NATURAL JOIN atelier WHERE id_intervenant = (select id_intervenant from intervenant where nom = 'Denis');
23) SELECT count(*) FROM (SELECT DISTINCT p.id_participant FROM participant p, intervenant i, cours c, inscription ii, atelier a WHERE p.id_participant = ii.id_participant AND i.id_intervenant = a.id_intervenant AND c.id_cours = ii.id_cours AND c.id_cours = a.id_cours AND (i.nom = 'Denis' OR i.nom = 'Amandine')) AS foo;
23 version 2) SELECT count(*) FROM (SELECT DISTINCT p.id_participant FROM intervenant i, atelier a, (SELECT * FROM participant NATURAL JOIN inscription NATURAL JOIN cours) p  WHERE i.id_intervenant = a.id_intervenant AND p.id_cours = a.id_cours AND (i.nom = 'Denis' OR i.nom = 'Amandine')) AS foo;
24) Select d.id_cours,d.danse FROM cours d, inscription i,participant p 
WHERE d.id_cours = i.id_cours AND p.id_participant = i.id_participant AND P.sexe = 'M' 
GROUP BY d.id_cours,d.danse HAVING count(p.sexe) > 
(Select count(p.sexe) FROM cours c, inscription i,participant p 
	WHERE c.id_cours = i.id_cours AND p.id_participant = i.id_participant AND P.sexe = 'F' AND c.id_cours = d.id_cours);
24 version 2) Select c.id_cours,c.danse FROM cours c NATURAL JOIN inscription NATURAL JOIN participant p 
WHERE sexe = 'M' 
GROUP BY c.id_cours,c.danse HAVING count(sexe) > 
(Select count(sexe) FROM cours d NATURAL JOIN inscription  NATURAL JOIN participant
	WHERE sexe = 'F' AND c.id_cours = d.id_cours);
