CREATE DATABASE centre_plongee;
CREATE TABLE niveau(
		numNiveau VARCHAR(6) NOT NULL,
		organisme VARCHAR(4) NOT NULL CHECK (organisme IN ('bpt', 'cmas', 'padi')),
		profondeurPE INTEGER NOT NULL,
		profondeurPA INTEGER NOT NULL,
		PRIMARY KEY (numNiveau, organisme));

CREATE TABLE vetementPlongee(
		nomVetement VARCHAR(30) NOT NULL,
		taille INTEGER NOT NULL,
		prixVetement INTEGER NOT NULL,
		quantite INTEGER,
		PRIMARY KEY (nomVetement, taille));

CREATE TABLE materiel(
		nomMateriel VARCHAR(30) NOT NULL,
		taille INTEGER NOT NULL,
		prixMateriel INTEGER NOT NULL,
		quantite INTEGER NOT NULL,
		PRIMARY KEY (nomMateriel, taille));

CREATE TABLE bateau(
		nomBateau VARCHAR(30) NOT NULL,
		capacite INTEGER NOT NULL,
		estDisponible BOOLEAN NOT NULL,
		PRIMARY KEY (NomBateau));

CREATE TABLE materielSecours(
		nomBateau VARCHAR(30) NOT NULL,
		nomMaterielSecours VARCHAR(30) NOT NULL,
		quantite INTEGER NOT NULL,
		FOREIGN KEY (nomBateau) REFERENCES bateau(nomBateau),
		PRIMARY KEY (nomBateau, nomMaterielSecours));

CREATE TABLE bouteille(
		idBouteille SERIAL PRIMARY KEY,
		volume REAL,
		materiaux VARCHAR(50),
		configuration VARCHAR(30),
		gaz VARCHAR(30));

CREATE TABLE site(
		nomSite VARCHAR(30) NOT NULL,
		typeSite VARCHAR(10) NOT NULL,
		coordonnees INTEGER NOT NULL,
		supplement INTEGER NOT NULL,
		prerogativeMinPE INTEGER NOT NULL,
		PRIMARY KEY (nomSite));


CREATE TABLE prestations(
		typePrestation VARCHAR(30) NOT NULL,
		prix INTEGER NOT NULL,
		PRIMARY KEY (typePrestation));

CREATE TABLE palanquee(
		numPalanquee SERIAL,
		numMoniteur INTEGER NULL,
		PRIMARY KEY (numPalanquee),
		FOREIGN KEY(numMoniteur) REFERENCES moniteur(numMoniteur)
		);

	CREATE TABLE plongeur(
			numPlongeur SERIAL,
			nom VARCHAR(30) NOT NULL,
			prenom VARCHAR(30) NOT NULL,
			dateNaissance DATE NOT NULL,
			email VARCHAR(50) NOT NULL,
			numTelephone INTEGER NOT NULL,
			adresse VARCHAR(50) NOT NULL,
			dateCertificatMedical DATE NOT NULL,
			resteAPayer INTEGER NOT NULL,
			dateLimite DATE NOT NULL,
			numNiveau INTEGER NOT NULL,
			organisme VARCHAR(4) NOT NULL,
			numPalanquee INTEGER NOT NULL,
			PRIMARY KEY (numPlongeur),
			FOREIGN KEY(numNiveau, organisme) REFERENCES niveau(numNiveau, organisme),
			FOREIGN KEY(numPalanquee) REFERENCES palanquee(numPalanquee));


CREATE TABLE moniteur(
		numMoniteur SERIAL,
		nom VARCHAR(30) NOT NULL,
		prenom VARCHAR(30) NOT NULL,
		PRIMARY KEY (numMoniteur));

CREATE TABLE appartenancePlongeurPalanquee(
		numPalanquee INTEGER NOT NULL,
		numPlongeur INTEGER NOT NULL,
		PRIMARY KEY(numPalanquee, numPlongeur),
		FOREIGN KEY(numPalanquee) REFERENCES palanquee(numPalanquee),
		FOREIGN KEY(numPlongeur) REFERENCES Plongeur(numPlongeur)
		);

CREATE TABLE sortie(
		dateSortie DATE NOT NULL,
		heureSortie TIME NOT NULL,
		nomBateau VARCHAR(30) NOT NULL,
		site VARCHAR(30) NOT NULL,
		PRIMARY KEY (dateSortie, heureSortie, nomBateau),
		FOREIGN KEY (nomBateau) REFERENCES Bateau(nomBateau),
		FOREIGN KEY (site) REFERENCES site(nomSite)
		);

CREATE TABLE reservation(
		dateSortie DATE NOT NULL,
		heureSortie TIME NOT NULL,
		nomBateau VARCHAR(30) NOT NULL,
		numPlongeur INTEGER NOT NULL,
		PRIMARY KEY (dateSortie, heureSortie, nomBateau, numPlongeur),
		FOREIGN KEY (dateSortie, heureSortie, nomBateau) REFERENCES Sortie(dateSortie, heureSortie, nomBateau),
		FOREIGN KEY (numPlongeur) REFERENCES Plongeur(numPlongeur)
		);

CREATE TABLE presenceMoniteurSortie(
		dateSortie DATE NOT NULL,
		heureSortie TIME NOT NULL,
		nomBateau VARCHAR(30) NOT NULL,
		numMoniteur INTEGER NOT NULL,
		PRIMARY KEY (dateSortie, heureSortie, nomBateau, numMoniteur),
		FOREIGN KEY (dateSortie, heureSortie, nomBateau) REFERENCES Sortie(dateSortie, heureSortie, nomBateau),
		FOREIGN KEY (numMoniteur) REFERENCES Moniteur(numMoniteur)
		);
