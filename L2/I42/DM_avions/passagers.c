#include "passagers.h"


void initShm(int* shmId, int bytes, int n) {
  key_t key = ftok("/tmp/mykey", n);
  *shmId = shmget(key, bytes, 420 | IPC_CREAT);
}

void releaseShm(int shmId) {
  shmctl(shmId, IPC_RMID, NULL);
}

