#include <stdlib.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>

/* Initialisation d'un segment de mémoire partagée de [bytes] octets, avec identifiant de clé: n */
void initShm(int* shmId, int bytes, int n);

/* Libère un segment de mémoire partagée */
void releaseShm(int shmId);

