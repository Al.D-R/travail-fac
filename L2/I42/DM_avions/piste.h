#include <stdlib.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>

/* Initialise un sémaphore */
void initSem(int* semId);

/* Libère un sémaphore */
void releaseSem(int semId);

/* Décrémente le sémaphore [semId]  */
void demanderPiste(int semId);

/* Incrémente le sémaphore [semId] */
void libererPiste(int semId);
