#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "piste.h"
#include "passagers.h"

/* Type de demande */
typedef enum {
  DECOLLAGE = 0,
  ATTERRISSAGE
} typeDemande;

/* IPCs */
int sem;
int shm;
long* shmAddr;

/* Gestion des avions: executée par les processus fils */
void gererAvion(typeDemande demande) {
  srand(getpid());
  int temps = rand() % 10 + 1;      /* Temps d'attente initial */
  int passagers = rand() % 256 + 1; /* Nombre de passagers */
  sleep(temps);

  /* Gestion du décollage */
  if (demande == DECOLLAGE) {
    printf("Vol numéro \033[93m%d\033[0m de \033[92m%d\033[0m passagers souhaitent \033[92mdécoller\033[0m\n", getpid(), passagers);
    fflush(stdout);
    demanderPiste(sem);
    printf("\tVol numéro \033[93m%d\033[0m en phase de \033[92mdécollage\033[0m\n", getpid());
    fflush(stdout);
    sleep(5); /* Temps de décollage */
    libererPiste(sem);
    printf("\t\tPiste libérée par vol \033[93m%d\033[0m\n", getpid());
    fflush(stdout);
  }
  /* Gestion de l'atterrissage */
  else {
    printf("Vol numéro \033[93m%d\033[0m de \033[91m%d\033[0m passagers souhaitent \033[91matterrir\033[0m\n", getpid(), passagers);
    fflush(stdout);
    demanderPiste(sem);
    printf("\tVol numéro \033[93m%d\033[0m en phase d'\033[91matterrissage\033[0m\n", getpid());
    fflush(stdout);
    sleep(3); /* Temps d'atterrissage */
    libererPiste(sem);
    printf("\t\tPiste libérée par vol \033[93m%d\033[0m\n", getpid());
    fflush(stdout);
  }
  /* Comptabilisation des passagers, index 0 -> passagers partant, index 1 -> passagers arrivants */
  shmAddr[demande] += passagers;
  exit(0);
}

int main(int argc, char** argv) {

  /* Vérification des arguments */
  if (argc < 3) {
    fprintf(stderr, "Veuillez préciser le nombre d'avions souhaitant décoller et atterrir\n");
    exit(1);
  }

  /* Création du ficher clé */
  FILE* f = NULL;
  f = fopen("/tmp/mykey", "w+");
  if (!f) {
    fprintf(stderr, "Errur, impossible de générer le fichier clé\n");
    exit(2);
  }
  fclose(f);

  /* Récupération des arguments sur la ligne de commande */
  int attenteDecollage = atoi(argv[1]);
  int attenteAtterrissage = atoi(argv[2]);


  /* Initialisation des IPCs */
  initSem(&sem);
  initShm(&shm, 2 * sizeof(long), 456);
  shmAddr = shmat(shm, NULL, 0);
  shmAddr[0] = 0;
  shmAddr[1] = 0;

  /* Gestion des avions souhaitants décoller */
  for (int i = 0; i < attenteDecollage; i++) {
    pid_t pid = fork();
    if (!pid) {
      gererAvion(DECOLLAGE);
    }
  }

  /* Gestion des avions souhaitants atterrir */
  for (int i = 0; i < attenteAtterrissage; i++) {
    pid_t pid = fork();
    if (!pid) {
      gererAvion(ATTERRISSAGE);
    }
  }

  /* Attente de la fin de tous les processus fils */
  for (int i = 0; i < attenteDecollage + attenteAtterrissage; i++)
    wait(NULL);

  /* Affichage du nombre de passagers arrivants et partants */
  printf("\nFin de journée\n");
  printf("Nombre de passagers partants: \033[92m%lu\033[0m\n", shmAddr[0]);
  printf("Nombre de passagers arrivants: \033[91m%lu\033[0m\n", shmAddr[1]);

  /* Libération des IPCs */
  releaseSem(sem);
  shmdt(shmAddr);
  releaseShm(shm);


  return 0;
}
