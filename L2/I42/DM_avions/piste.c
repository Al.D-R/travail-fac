#include "piste.h"


void initSem(int* semId) {
  key_t key = ftok("/tmp/mykey", 123);
  *semId = semget(key, 1, 420 | IPC_CREAT);
  union semun {
    int val;
    struct semid_ds *buf;
    unsigned short  *array;
    struct seminfo  *__buf;
    unsigned short  semval;
  } args;
  /* Initialise le sémaphore à 1 */
  args.val = 1;
  semctl(*semId, 0, SETVAL, args);
}

void releaseSem(int semId) {
  semctl(semId, 0, IPC_RMID, NULL);
}

void demanderPiste(int semId) {
  struct sembuf buf = { 0, -1, 0};
  semop(semId, &buf, 1);
}

void libererPiste(int semId) {
  struct sembuf buf = { 0, 1, 0};
  semop(semId, &buf, 1);
}
