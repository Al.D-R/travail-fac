# coding: utf-8

"""
Serveur retournant l'age en fonction d'une année de naissance
"""

import socket
import sys
import datetime

HOST = '127.0.0.1'
PORT = 4567

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Creation du socket

# Liaison du socket à l'adresse et au port
try:
   mySocket.bind((HOST, PORT))
except socket.error:
   print("Liaison du socket impossible")
   sys.exit(1)

# Ecoute permanente
while True:
    correct = True # On s'assure que la valeur est correcte
    mySocket.listen(5) # Ecoute

    connexion, adresse = mySocket.accept() # Recupère la nouvelle connexion et l'adresse
    print("Nouvelle connexion")

    connexion.send(("Quelle est votre annee de naissance ?\n").encode('UTF-8')) # Question

    annee = connexion.recv(1024) # Reception des données de l'utilisateur
    annee = annee.decode("utf-8")

    # On s'assure que la chaine reçue peut être convertie en entier
    try:
        annee = int(annee)
    except ValueError:
        connexion.send(("Veuillez saisir une annee valide\n").encode("UTF-8"))
        correct = False

    # On s'assure que l'année reçue n'est pas supérieur à l'année actuelle 
    currentDate = datetime.date.today()
    if correct and annee > currentDate.year:
        connexion.send(("Vous arrivez du futur depuis une Dolorean ?\n").encode("UTF-8"))
        correct = False
    elif correct:
        age = currentDate.year - annee # Calcule de l'age
        connexion.send(("Vous avez " + str(age) + " ans.\n").encode('UTF-8')) # Réponse si tout est correcte

    print("Connexion interrompue.")
    connexion.close()
