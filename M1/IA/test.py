from scipy.optimize import minimize

data = [0 for i in range(25)]
data[5] = 0.1085
data[10] = 0.1035
data[11] = 0.1032
data[15] = 0.1023
data[16] = 0.1056
data[17] = 0.1075
data[20] = 0.1912
data[21] = 0.1925
data[22] = 0.1255
data[23] = 0.1250

def f(arr):
    pass

res = minimize(f, data)
print(res.x)
