#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <immintrin.h>

void debug(char *s) {
	printf("[%c,%c,%c,%c,%c,%c,%c,%c,%c,%c,%c,%c,%c,%c,%c,%c]\n",
			 s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7], 
			 s[8], s[9], s[10], s[11], s[12], s[13], s[14], s[15]);
}

char* cesar(char* s, char k) {
	unsigned int n = strlen(s);
	char *res = calloc(n / 16 + 1, sizeof(char));
	__m128i reg_k = _mm_set_epi8(k, k, k, k, k, k, k, k, k, k, k, k, k, k, k, k);
	for (int i = 0; i < n; i += 16) {
		char tmp[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		int j = 0;
		for (j = 0; j < 16; j++) tmp[j] = s[i+j] - 'A';		
		__m128i reg_s = _mm_set_epi8(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7], 
				tmp[8], tmp[9], tmp[10], tmp[11], tmp[12], tmp[13], tmp[14], tmp[15]);
		reg_s = _mm_add_epi8
	}
	return res;
}

int main(int argc, char* argv[]) {

	char *truc = cesar("ABCABCABCABCABCABCABC", 3);
	for (int i = 0; i < strlen(truc); i++)
		printf("res: %c ", truc[i]);
	printf("\n");
	free(truc);

	return 0;
}
