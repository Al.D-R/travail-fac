import random

def traitementChaine(s):
    res = ""
    s = s.lower()
    for c in s:
        if c == 'é' or c == 'è' or c == 'ê' or c == 'ë':
            res += 'e'
        elif c == 'à' or c == 'â':
            res += 'a'
        elif c == 'ç':
            res += 'c'
        elif c == 'ù' or c == 'û' or c == 'ü':
            res += 'u'
        elif c == 'î' or c == 'ï':
            res += 'i'
        elif c == 'ô' or c == 'ò':
            res += 'o'
        elif c == 'œ':
            res += "oe"
        elif c.isalpha():
            res += c
    return res.upper()

def nb_apparitions(s, out = ""):
    n = len(s)
    res = {k:0.0 for k in [chr(i) for i in range(ord('A'), ord('Z')+1)]}
    for c in s:
        res[c] += 1
    for k in res:
        res[k] = res[k] * 100 / n
    if out != "":
        f = open(out, "w")
        for k in res:
            f.write("{}\t{}%\n".format(k, res[k]))
        f.close
    return res

def cesar(s, k):
    if k < 0:
        k = random.randint(0, 25)
    n = len(s)
    res = ""
    for c in s:
        t = ((ord(c) - ord('A') + k) % 26) + ord('A')
        res += chr(t)
    return res

def decesar(s, k):
    n = len(s)
    res = ""
    for c in s:
        t = ((ord(c) - ord('A') - k) % 26) + ord('A')
        res += chr(t)
    return res

def frequences(filename):
    f = open(filename, "r")
    res = {}
    i = 0
    for line in f:
        c = chr(i + ord('A'))
        res[c] = float(line)
        i += 1
    f.close()
    return res

def coincidance(s):
    n = len(s)
    res = 0.0
    freq = frequences("freq.txt")
    for k in freq:
        res += freq[k] ** 2
    return res / 10000 # Correction pourcentage carre


# 1.3.3
# inf = open("In.txt", "r")
# outf = open("Out.txt", "w")
# k = int(input("Clef: "))
# s = inf.read()
# outf.write(cesar(traitementChaine(s), k))
# inf.close()
# outf.close()


# 1.4.1
# inf = open("In.txt", "r")
# s = inf.read()
# print(coincidance(traitementChaine(s)))
# inf.close()

# L'indice de coincidence est le meme malgre le chiffrement de Cesar
# On peut en connaissant les indices de coincidence determiner la langue d'origine du texte

def cryptanalyseCesar(s):
    freq_fr = frequences("freq.txt")
    freq_txt = nb_apparitions(s)
    correspondance = {}
    # Etablit la correspondance entre les lettres du clair et du chiffre
    for k1 in freq_fr:
        r = k1
        minimum = 1
        freq_x = freq_fr[k1]
        for k2 in freq_txt:
            f = freq_txt[k2]
            d = abs(freq_x - f)
            if d < minimum:
                minimum = d
                r = k2
        correspondance[k1] = r
    # Recupere la clef la plus probable
    keys = {}
    for k in correspondance:
        d = (ord(correspondance['E']) - ord('E')) % 26
        if d in keys:
            keys[d] += 1
        else:
            keys[d] = 1
    res = 0
    maximum = 0
    for k in keys:
        if keys[k] > minimum:
            maximum = keys[k]
            res = k
    return res

inf = open("Out.txt", "r")
s = traitementChaine(inf.read())
print(cryptanalyseCesar(s))
inf.close()
