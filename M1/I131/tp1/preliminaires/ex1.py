
# 1.2
def traitementChaine(s):
    res = ""
    s = s.lower()
    for c in s:
        if c == 'é' or c == 'è' or c == 'ê' or c == 'ë':
            res += 'e'
        elif c == 'à' or c == 'â':
            res += 'a'
        elif c == 'ç':
            res += 'c'
        elif c == 'ù' or c == 'û' or c == 'ü':
            res += 'u'
        elif c == 'î' or c == 'ï':
            res += 'i'
        elif c == 'ô' or c == 'ò':
            res += 'o'
        elif c == 'œ':
            res += "oe"
        elif c.isalpha():
            res += c
    return res.upper()

# 1.2.3
# f = open("93.txt", "r")
# s = f.read()
# out = open("93out.txt", "w")
# out.write(traitementChaine(s))
# out.close()
# f.close()

# 1.2.5
def nb_apparitions(s, out = ""):
    n = len(s)
    res = {k:0.0 for k in [chr(i) for i in range(ord('A'), ord('Z')+1)]}
    for c in s:
        res[c] += 1
    for k in res:
        res[k] = res[k] * 100 / n
    if out != "":
        f = open(out, "w")
        for k in res:
            f.write("{}\t{}%\n".format(k, res[k]))
        f.close
    return res

# TODO matplotlib
# f = open("93.txt", "r")
# s = f.read()
# nb_apparitions(traitementChaine(s), "out.txt")
# f.close()

def nb_digrammes(s):
    res = {'ES': 0, 'DE': 0, 'LE': 0, 'EN': 0, 'RE': 0, 'NT': 0, 'ON': 0, 'ER': 0, 'TE': 0, 'EL': 0, 'AN': 0, 'SE': 0, 'ET': 0, 'LA': 0, 'AI': 0, 'IT': 0, 'ME': 0, 'OU': 0, 'EM': 0, 'IE': 0}
    n = len(s)
    if n < 2: return res
    for i in range(n - 2):
        d = s[i] + s[i+1]
        if d in res: res[d] += 1
    return res

